#!/usr/bin/env node --harmony
/* jshint indent: 2 */
/*
 * media-vault-import tool
 * Organize your media files following strict compliance and opinionated standardization
 * Rolando Lora
 */

const _ = require("underscore");
const program = require("commander");
const fs = require("fs");
const shell = require("shelljs");
const path = require("path");
const colors = require("colors");
const exif = require("simple-exiftool");
const moment = require("moment");
const crypto = require("crypto");
const async = require("async");

//
// Set of valid extensions
//
const validExtensions = {
  ".pdf": "Portable Document Format",
  ".avi": "Microsoft Audio/Visual Interleaved",
  ".dng": "Digital Negative",
  ".tif": "Digital Negative",
  ".jpg": "Joint Photographic Experts Group JFIF format",
  ".mov": "QuickTime Movie",
  ".mp4": "MPEG-4 Video Stream",
  ".mp3": "MP3 Audio",
  ".mts": "AVCHD Video File",
  ".rw2": "Panasonic RAW Image file",
  ".wav": "Waveform audio file"
};

//
// Results summary
//
const globalResults = {
  invalidFileCount: 0,
  inputQueueSize: 0,
  successCount: 0,
  duplicateCount: 0,
  failedCount: 0
};

//
// Program setup
//
program
  .version("0.0.1")
  .option("-o, --output <path>", "Add media vault path")
  .option("-m, --move", "Move media file instead of making a copy")
  .option("-s, --simulate", "Simulate action to determine behavior but do not move/copy files")
  .arguments("<input>");

//
// Main program loop
//
program.action(input => {

  //
  // Validate output path
  //
  if (!program.output) {
    console.log("\nPlease specify output path!".red);
    program.help();
    return;
  } else {
    try {
      const stat = fs.statSync(program.output);
      if (stat.isDirectory() === false) {
        throw "Invalid directory";
      }
    } catch(e) {
      console.log("\nSpecified output is not a valid directory!".red);
      program.help();
      return;
    }
  }
  //
  // Preprocess input list to find target files that exist and belong
  // to one of the allowed extensions.
  //
  const files = input.split("\n");
  const verifiedFiles = [];
  files.forEach(file => {
    const basename = path.basename(file);
    const extname = path.extname(file).toLowerCase();
    try {
      const stat = fs.statSync(file);
      if (validExtensions[extname]) {
        globalResults.inputQueueSize += 1;
        verifiedFiles.push(file);
      }
    } catch(e) {
      globalResults.invalidFileCount += 1;
    }
  });
  //
  // Begin processing loop
  //
  var tasks = [];
  console.log("Output directory:".white, program.output.blue);
  console.log("Invalid input file count:".white, `${globalResults.invalidFileCount}`.red);
  console.log("Valid input file count:".white, `${verifiedFiles.length}`.green);
  console.log("-".repeat(100).green);
  console.log("- IMPORT -".repeat(10).green);
  console.log("-".repeat(100).green);
  verifiedFiles.forEach(file => {
    const basename = path.basename(file);
    const extname = path.extname(file).toLowerCase();
    tasks.push(callback => {
      exif(file, (error, data) => {
        if (error) {
          globalResults.failedCount += 1;
          callback(error);
        }
        var shasum = shell.exec("shasum '" + file + "' | awk '{ print $1 }'", {silent: true});
        if (shasum.code !== 0) {
          globalResults.failedCount += 1;
          callback("Invalid shasum execution");
        }
        shasum = shasum.stdout.slice(0, -1); // Strip end of line
        const imageSize = data.ImageSize ? "-" + data.ImageSize : "";
        const dateAsString = data.TrackCreateDate || data.DateTimeOriginal || data.CreateDate || data.FileModifyDate || "";
        const when = moment(dateAsString, "YYYY:MM:DD hh:mm");
        const targetName = shasum + imageSize + extname;
        var targetPath = path.join(program.output, extname.replace(".", ""), "unknown");
        if (when.isValid()) {
          targetPath = path.join(program.output,
            extname.replace(".", ""),
            when.format("YYYY"),
            when.format("MM"),
            when.format("DD"));
        }

        //
        // Full path
        //
        const targetFullPath = path.join(targetPath, targetName);
        //
        // Create path if it doesn't exist
        //
        if (!shell.test("-d", targetPath)) {
          shell.mkdir("-p", targetPath);
          console.log(`Vault path didn't exist. Creating ${targetPath}`.green);
        }
        if (shell.test("-f", targetFullPath)) {
          console.log(`Duplicate file in vault. Skipping ${targetName}`.yellow);
          globalResults.duplicateCount += 1;
          shell.chmod(644, targetFullPath); // Still fixes permissions
          callback(null, "skip");
        } else {
          if (!program.simulate) {
            if (program.move) {
              console.log(`Move ${targetName} to vault`.yellow);
              shell.mv(file, targetFullPath);
            } else {
              console.log(`Copy ${targetName} to vault`.yellow);
              shell.cp(file, targetFullPath);
            }
            shell.chmod(644, targetFullPath);
          } else {
            console.log(`SIMULATION enabled. Won't do anything with ${targetName}`.yellow);
          }
          globalResults.successCount += 1;
          callback(null, "success");
        }
      });
    });
  });
  async.parallel(tasks, (error, results) => {
    console.log("-".repeat(100).green);
    console.log("- FINISH -".repeat(10).green);
    console.log("-".repeat(100).green);
    console.log(`Invalid count:\t\t${globalResults.invalidFileCount}`.white);
    console.log(`Queue size:\t\t${globalResults.inputQueueSize}`.white);
    console.log(`Success count:\t\t${globalResults.successCount}`.white);
    console.log(`Duplicate count:\t${globalResults.duplicateCount}`.white);
    console.log(`Failed count:\t\t${globalResults.failedCount}`.white);
  });
});

//
// Parse arguments
//
program.parse(process.argv);

//
// Show help
//
if (!program.args.length) {
  program.help();
}

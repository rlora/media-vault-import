# Media vault import

Command line utility intended to scan locations for media files and consolidate them into a **unified media vault**. Highly opinionated in terms of structure.

## Requires exiftool

`exiftool` must be properly installed in order to retrieve media info (dates, resolution). You can grab it here: http://www.sno.phy.queensu.ca/~phil/exiftool/

## Usage

```
1$ media-vault-import -o /my-vault photo.jpg # COPY
2$ media-vault-import -o /my-vault -m photo.jpg # MOVE
3$ find /Pictures -name "*.RW2" | xargs -0 media-vault-import -o /my-vault
4$ find /Pictures -name "*.RW2" | xargs -L 1 media-vault-import -o /my-vault
```

## TODO

  * Example (3) sometimes fail with a big list of files. Parallel execution causes a deadlock. (4) always work put it's not optimized for performance.
